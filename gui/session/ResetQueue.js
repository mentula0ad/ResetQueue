class ResetQueue
{

    resetAll()
    {
        g_Selection.toList().forEach(id => GetEntityState(id).production?.queue?.forEach(item => removeFromProductionQueue(id, item.id)));
    }

    resetTrail()
    {
        g_Selection.toList().forEach(id => GetEntityState(id).production?.queue?.slice(1).forEach(item => removeFromProductionQueue(id, item.id)));
    }

}
