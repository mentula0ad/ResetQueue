# About ResetQueue

*ResetQueue* is a [0 A.D.](https://play0ad.com) mod providing new hotkeys for clearing production queue items of selected buildings (and entities in general).

# Features
- Hotkey to reset production queues of selected entities.
- Hotkey to reset production queues of selected entities except the current item.

# Installation

[Click here](https://gitlab.com/mentula0ad/ResetQueue/-/releases/permalink/latest/downloads/resetqueue.pyromod) to download the latest release. Install following the official 0 A.D. guide: [How to install mods?](https://trac.wildfiregames.com/wiki/Modding_Guide#Howtoinstallmods)

_Alternative downloads:_ [Latest Release (.pyromod)](https://gitlab.com/mentula0ad/ResetQueue/-/releases/permalink/latest/downloads/resetqueue.pyromod) | [Latest Release (.zip)](https://gitlab.com/mentula0ad/ResetQueue/-/releases/permalink/latest/downloads/resetqueue.zip) | [Older Releases](https://gitlab.com/mentula0ad/ResetQueue/-/releases)

# Questions & feedback

For more information, questions and feedback, visit the [thread on the 0 A.D. forum](https://wildfiregames.com/forum/topic/80176-resetqueue-mod-a-hotkey-for-clearing-production-queues-of-selected-buildings/).
